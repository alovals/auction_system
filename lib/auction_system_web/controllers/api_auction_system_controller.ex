defmodule AuctionSystemWeb.API.AuctionSystemController do
    use AuctionSystemWeb, :controller
    import Ecto.Changeset
    alias AuctionSystem.{Repo, Auction}

    def get_auctions(conn, _params) do
        auctions = Repo.all(Auction)
        put_status(conn, 201)
            |> json(%{openAuctions: auctions})
    end

    def set_new_price(conn, %{"params" => %{"id" => id, "newBid" => newBid}}) do
        case Repo.get_by(Auction, id: id) do 
            nil -> put_status(conn, 400)
                |> json(%{msg: "Auction not found"})
            record -> 
                case Float.parse(record.price) < Float.parse(newBid) do
                    true ->
                        Repo.update(change record, %{price: newBid})
                        put_status(conn, 201)
                        |> json(%{msg: "New price is set"})
                    false ->
                        put_status(conn, 400)
                        |> json(%{msg: "Bad bid, discarded"})
                end
        end
    end


end