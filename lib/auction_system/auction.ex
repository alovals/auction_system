defmodule AuctionSystem.Auction do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Poison.Encoder, only: [:id, :closing_date, :description, :price]}
  schema "auctions" do
    field :closing_date, :string
    field :description, :string
    field :price, :string

    timestamps()
  end

  @doc false
  def changeset(auction, attrs) do
    auction
    |> cast(attrs, [:description, :price, :closing_date])
    |> validate_required([:description, :price, :closing_date])
  end
end
