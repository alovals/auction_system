defmodule AuctionSystem.Repo.Migrations.CreateAuctions do
  use Ecto.Migration

  def change do
    create table(:auctions) do
      add :description, :string
      add :price, :string
      add :closing_date, :string

      timestamps()
    end

  end
end
