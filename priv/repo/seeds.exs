alias AuctionSystem.Repo
alias AuctionSystem.Auction

Repo.insert!(%Auction{description: "Roller Derby Brond Blade Skate (Size 7)", price: "29.00", closing_date: "08/01/2019"})
Repo.insert!(%Auction{description: "Chicago Bulelt Speed Skate (Size7)", price: "59.00", closing_date: "08/01/2019"})
Repo.insert!(%Auction{description: "Riedell Dart Derby Skates (Size8)", price: "106.00", closing_date: "08/01/2019"})