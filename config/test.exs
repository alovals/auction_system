use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :auction_system, AuctionSystemWeb.Endpoint,
  http: [port: 4001],
  server: true

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :auction_system, AuctionSystem.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "auction_system_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox


config :hound, driver: "chrome_driver"
config :auction_system, sql_sandbox: true