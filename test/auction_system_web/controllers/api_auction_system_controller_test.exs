defmodule AuctionSystemWeb.API.AuctionSystemControllerTest do
    use AuctionSystemWeb.ConnCase
  
    alias AuctionSystem.{Repo, Auction}

    test "POST /api/bid success", %{conn: conn} do
        auc_id = Repo.insert!(%Auction{description: "Chicago Bulelt Speed Skate (Size7)", price: "59.00", closing_date: "08/01/2019"}).id;
        request = %{"params" => %{"id" => auc_id, "newBid" => "62.00"}}
        response =
            conn
            |> post(auction_system_path(conn, :set_new_price, request))
            |> json_response(201)
    
        expected = %{"msg" => "New price is set"}
    
        assert response == expected
      end

end