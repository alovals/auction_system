defmodule AuctionSystemWeb.PageControllerTest do
  use AuctionSystemWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "Hello AuctionSystem!"
  end
end
