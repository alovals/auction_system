Feature: Open auctions
  As a bidder
  Such that I want to place a bid
  I want to see open auctions list

    Scenario: Place an acceptable bid
        Given the following auctions are open
            | description                                 | price	    | closing_date  |
            | Roller Derby Brond Blade Skate (Size 7)     | 29.00     | 08/01/2019	|
            | Chicago Bulelt Speed Skate (Size7)          | 59.00     | 08/01/2019	|  
            | Riedell Dart Derby Skates (Size8)           | 106.00    | 08/01/2019	|
        And I open web page
        And I select the auction of "Chicago Bulelt Speed Skate (Size7)"
        And I enter the pid of "62.00"
        When I submit the pid
        Then the current price shouldbe updated to "62.00"

    Scenario: Place an discardable bid   
        Given the following auctions are open
            | description                                 | price	    | closing_date  |
            | Roller Derby Brond Blade Skate (Size 7)     | 29.00     | 08/01/2019	|
            | Chicago Bulelt Speed Skate (Size7)          | 59.00     | 08/01/2019	|  
            | Riedell Dart Derby Skates (Size8)           | 106.00    | 08/01/2019	|
        And I open web page
        And I select the auction of "Riedell Dart Derby Skates (Size8)"
        And I enter the pid of "50.00"
        When I submit the pid
        Then the pid is rejected