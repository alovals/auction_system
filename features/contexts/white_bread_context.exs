defmodule WhiteBreadContext do
  use WhiteBread.Context
  use Hound.Helpers
  
  feature_starting_state fn  ->
    Application.ensure_all_started(:hound)
    %{}
  end
  scenario_starting_state fn _state ->
    Hound.start_session
    %{}
  end
  scenario_finalize fn _status, _state -> 
    #Hound.end_session
    nil
  end

  #steps

  given_ ~r/^the following auctions are open$/, fn state ->
    {:ok, state}
  end
  #   {:ok, state |> Map.put({
  #     :description, "Roller Derby Brond Blade Skate (Size 7)",
  #     :price, 29.00,
  #     :closing_date, "08/01/2019"
  #   }, {
  #     :description, "Chicago Bulelt Speed Skate (Size7)",
  #     :price, 59.00,
  #     :closing_date, "08/01/2019"
  #   },{
  #     :description, "Riedell Dart Derby Skates (Size8)",
  #     :price, 106.00,
  #     :closing_date, "08/01/2019"
  #   })}
  # end

  and_ ~r/^I open web page$/, fn state ->
    navigate_to "/"
    {:ok, state}
  end

  and_ ~r/^I select the auction of "(?<auction_name>[^"]+)"$/,
  fn state, %{auction_name: auction_name} ->
    {:ok, state |> Map.put(:auction_name, auction_name)}
  end

  and_ ~r/^I enter the pid of "(?<new_bid>[^"]+)"$/,
  fn state, %{new_bid: new_bid} ->
    {:ok, state |> Map.put(:new_bid, new_bid)}
  end

  when_ ~r/^I submit the pid$/, fn state ->
    {:ok, state}
  end

  then_ ~r/^the current price shouldbe updated to "(?<new_price>[^"]+)"$/,
  fn state, %{new_price: new_price} ->
    {:ok, state |> Map.put(:new_price, new_price)}
  end

  then_ ~r/^the pid is rejected$/, fn state ->
    {:ok, state}
  end
end
